## Description

### ABLA

Reads a sequence of block sizes from standard input and outputs a .csv formatted sequence of `blockHeight,blockSize,blockSizeLimit[,algorithmInternalState]` to standard output.

Algorithms:

- abla-median
- abla-dual-median
- abla-wtema-elastic-buffer
- abla-wtema-elastic-buffer-bounded

### Aggregate

Reads a .csv formatted sequence of `blockHeight,blockSize,blockSizeLimit` from standard input and outputs .csv formatted statistical aggregates for the specified window length to standard output.

## Build Instructions

`./make.sh`
