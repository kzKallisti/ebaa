// abla-ewma-elastic-buffer
//
// Originally written by bitcoincashautist but converted to C++ by Calin Culianu.
// LICENSE: MIT
//
#include <algorithm>
#include <cassert>
#include <cinttypes>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <string_view>

namespace ABLA {

// Post-filter set to 2 GB due to limitation of 32-bit architectures
// block storage.
// When 32-bit will be deprecated, it should be entirely removed.
uint64_t TEMP_32_BIT_MAX_SAFE_BLOCKSIZE_LIMIT = 2000000000;

// Utility function
static inline uint64_t muldiv(uint64_t x, uint64_t y, uint64_t z) {
    assert(z != 0);
    using uint128_t = __uint128_t ;
    const uint128_t res = (static_cast<uint128_t>(x) * static_cast<uint128_t>(y)) / static_cast<uint128_t>(z);
    assert(res <= static_cast<uint128_t>(std::numeric_limits<uint64_t>::max()));
    return static_cast<uint64_t>(res);
}

// Algorithm configuration
struct Config {
    // Initial control block size value, also used as floor value
    uint64_t epsilon0{};
    // Initial elastic buffer size value, also used as floor value
    uint64_t beta0{};
    // Last block height which will have the initial block size limit
    uint64_t n0{};
    // Reciprocal of control function "forget factor" value
    uint64_t gammaReciprocal{};
    // Control function "asymmetry factor" value
    uint64_t zeta_xB7{};
    // Reciprocal of elastic buffer decay rate
    uint64_t thetaReciprocal{};
    // Elastic buffer "gear factor"
    uint64_t delta{};
    // Maximum control block size value
    uint64_t epsilonMax{};
    // Maximum elastic buffer size value
    uint64_t betaMax{};

    // Constant 2^7, used as fixed precision for algorithm's "asymmetry
    // factor" configuration value, e.g. we will store the real number 1.5
    // as integer 192 so when we want to multiply or divide an integer with
    // value of 1.5, we will do muldiv(value, 192, B7) or
    // muldiv(value, B7, 192).
    static constexpr const uint64_t B7 = 1u << 7u;

    // Sanity ranges for configuration values
    static constexpr const uint64_t MIN_ZETA_XB7 = 129u; // zeta real value of 1.0078125
    static constexpr const uint64_t MAX_ZETA_XB7 = 256u; // zeta real value of 2.0000000
    static constexpr const uint64_t MIN_GAMMA_RECIPROCAL = 9484u;
    static constexpr const uint64_t MAX_GAMMA_RECIPROCAL = 151744u;
    static constexpr const uint64_t MIN_DELTA = 0u;
    static constexpr const uint64_t MAX_DELTA = 32u;
    static constexpr const uint64_t MIN_THETA_RECIPROCAL = 9484u;
    static constexpr const uint64_t MAX_THETA_RECIPROCAL = 151744u;

    // Set epsilonMax and betaMax such that algo's internal arithmetic ops can't overflow UINT64_MAX
    void SetMax(void);

    // Returns true if the configuration is valid and/or sane. On false return optional out `*err` is set to point to
    // a constant string explaining the reason that it is invalid.
    [[nodiscard]] bool IsValid(const char **err = nullptr) const;
};

// Set epsilonMax and betaMax such that algo's internal arithmetic ops can't overflow UINT64_MAX
void Config::SetMax(void) {
    uint64_t maxSafeBlocksizeLimit = std::numeric_limits<uint64_t>::max() / zeta_xB7 * B7;

    // elastic_buffer_ratio_max = (delta * gamma / theta * (zeta - 1)) / (gamma / theta * (zeta - 1) + 1)
    uint64_t maxElasticBufferRatioNumerator = delta * ((zeta_xB7 - B7) * thetaReciprocal / gammaReciprocal);
    uint64_t maxElasticBufferRatioDenominator = (zeta_xB7 - B7) * thetaReciprocal / gammaReciprocal + B7;
    
    epsilonMax = maxSafeBlocksizeLimit / (maxElasticBufferRatioNumerator + maxElasticBufferRatioDenominator) * maxElasticBufferRatioDenominator;
    betaMax = maxSafeBlocksizeLimit - epsilonMax;

    fprintf(stderr, "[INFO] Auto-configured epsilonMax: %lu, betaMax: %lu\n", epsilonMax, betaMax);
}

bool Config::IsValid(const char **err) const {
    if (epsilon0 > epsilonMax) {
        if (err) *err = "Error, initial control block size limit sanity check failed (epsilonMax)";
        return false;
    }
    if (beta0 > betaMax) {
        if (err) *err = "Error, initial elastic buffer size sanity check failed (betaMax).";
        return false;
    }
    if (zeta_xB7 < MIN_ZETA_XB7 || zeta_xB7 > MAX_ZETA_XB7) {
        if (err) *err = "Error, zeta sanity check failed.";
        return false;
    }
    if (gammaReciprocal < MIN_GAMMA_RECIPROCAL || gammaReciprocal > MAX_GAMMA_RECIPROCAL) {
        if (err) *err = "Error, gammaReciprocal sanity check failed.";
        return false;
    }
    if (delta + 1 <= MIN_DELTA || delta > MAX_DELTA) {
        if (err) *err = "Error, delta sanity check failed.";
        return false;
    }
    if (thetaReciprocal < MIN_THETA_RECIPROCAL || thetaReciprocal > MAX_THETA_RECIPROCAL) {
        if (err) *err = "Error, thetaReciprocal sanity check failed.";
        return false;
    }
    if (epsilon0 < muldiv(gammaReciprocal, B7, zeta_xB7 - B7)) {
        // Required due to truncation of integer ops.
        // With this we ensure that the control size can be adjusted for at least 1 byte.
        // Also, with this we ensure that divisior bytesMax in calculateNextABLAState() can't be 0.
        if (err) *err = "Error, epsilon0 sanity check failed. Too low relative to gamma and zeta.";
        return false;
    }
    if (err) *err = "";
    return true;
}

// Algorithm's internal state
// Note: limit for the block with blockHeight will be given by
// controlBlockSize + elasticBufferSize
struct State {
    // Block height for which the state applies
    uint64_t blockHeight{};
    // Control function state
    uint64_t controlBlockSize{};
    // Elastic buffer function state
    uint64_t elasticBufferSize{};

    // Calculate the limit for the block to which the algorithm's state
    // applies, given algorithm state
    uint64_t getBlockSizeLimit() const { return std::min(controlBlockSize + elasticBufferSize, TEMP_32_BIT_MAX_SAFE_BLOCKSIZE_LIMIT); }
    // Note: Remove the TEMP_32_BIT_MAX_SAFE_BLOCKSIZE_LIMIT limit once 32-bit architecture is deprecated:
    // uint64_t getBlockSizeLimit() const { return controlBlockSize + elasticBufferSize; }

    // Calculate algorithm's state for the next block (n), given
    // current blockchain tip (n-1) block size, algorithm state, and
    // algorithm configuration.
    State nextABLAState(const Config &config, uint64_t currentBlockSize) const;

    // Calculate algorithm's look-ahead block size limit, for a block N blocks ahead of current one.
    // Returns the limit for block with current+N height, assuming all blocks 100% full.
    State lookaheadState(const Config &config, size_t count);

    // Returns true if this state is valid relative to `config`. On false return, optional out `err` is set
    // to point to a constant string explaining the reason that this state is invalid.
    [[nodiscard]] bool IsValid(const Config &config, const char **err = nullptr) const;
};

// Calculate algorithm's state for the next block (n), given
// current blockchain tip (n-1) block size, algorithm state, and
// algorithm configuration. Returns the next state after this block.
State State::nextABLAState(const Config &config, uint64_t currentBlockSize) const {
    // Next block DatumABLA
    State ret;

    // n = n + 1
    ret.blockHeight = this->blockHeight + 1ull;

    // For safety: we clamp this current block's blocksize to the maximum value this algorithm expects. Normally this
    // won't happen unless the node is run with some -excessiveblocksize parameter that permits larger blocks than this
    // algo's current state expects.
    currentBlockSize = std::min(currentBlockSize, controlBlockSize + elasticBufferSize);

    // use initialization values for block heights 0 to n0 inclusive
    if (ret.blockHeight <= config.n0) {
        // epsilon_n = epsilon_0
        ret.controlBlockSize = config.epsilon0;
        // beta_n = beta_0
        ret.elasticBufferSize = config.beta0;
    }
    // algorithmic limit
    else {
        // control function

        // zeta * x_{n-1}
        const uint64_t amplifiedCurrentBlockSize = muldiv(config.zeta_xB7, currentBlockSize, config.B7);

        // if zeta * x_{n-1} > epsilon_{n-1} then increase
        if (amplifiedCurrentBlockSize > this->controlBlockSize) {
            // zeta * x_{n-1} - epsilon_{n-1}
            const uint64_t bytesToAdd = amplifiedCurrentBlockSize - this->controlBlockSize;

            // zeta * y_{n-1}
            const uint64_t amplifiedBlockSizeLimit = muldiv(config.zeta_xB7, controlBlockSize + elasticBufferSize, config.B7);

            // zeta * y_{n-1} - epsilon_{n-1}
            const uint64_t bytesMax = amplifiedBlockSizeLimit - this->controlBlockSize;

            // zeta * beta_{n-1} * (zeta * x_{n-1} - epsilon_{n-1}) / (zeta * y_{n-1} - epsilon_{n-1})
            const uint64_t scalingOffset = muldiv(muldiv(config.zeta_xB7, this->elasticBufferSize, config.B7),
                                                  bytesToAdd, bytesMax);

            // epsilon_n = epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1} - zeta * beta_{n-1} * (zeta * x_{n-1} - epsilon_{n-1}) / (zeta * y_{n-1} - epsilon_{n-1}))
            ret.controlBlockSize = this->controlBlockSize + (bytesToAdd - scalingOffset) / config.gammaReciprocal;
        }
        // if zeta * x_{n-1} <= epsilon_{n-1} then decrease or no change
        else {
            // epsilon_{n-1} - zeta * x_{n-1}
            const uint64_t bytesToRemove = this->controlBlockSize - amplifiedCurrentBlockSize;

            // epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1})
            // rearranged to:
            // epsilon_{n-1} - gamma * (epsilon_{n-1} - zeta * x_{n-1})
            ret.controlBlockSize = this->controlBlockSize - bytesToRemove / config.gammaReciprocal;

            // epsilon_n = max(epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1}), epsilon_0)
            ret.controlBlockSize = std::max(ret.controlBlockSize, config.epsilon0);
        }

        // elastic buffer function

        // beta_{n-1} * theta
        const uint64_t bufferDecay = this->elasticBufferSize / config.thetaReciprocal;

        // if zeta * x_{n-1} > epsilon_{n-1} then increase
        if (amplifiedCurrentBlockSize > this->controlBlockSize) {
            // (epsilon_{n} - epsilon_{n-1}) * delta
            const uint64_t bytesToAdd = (ret.controlBlockSize - this->controlBlockSize) * config.delta;

            // beta_{n-1} - beta_{n-1} * theta + (epsilon_{n} - epsilon_{n-1}) * delta
            ret.elasticBufferSize = this->elasticBufferSize - bufferDecay + bytesToAdd;
        }
        // if zeta * x_{n-1} <= epsilon_{n-1} then decrease or no change
        else {
            // beta_{n-1} - beta_{n-1} * theta
            ret.elasticBufferSize = this->elasticBufferSize - bufferDecay;
        }
        // max(beta_{n-1} - beta_{n-1} * theta + (epsilon_{n} - epsilon_{n-1}) * delta, beta_0) , if zeta * x_{n-1} > epsilon_{n-1}
        // max(beta_{n-1} - beta_{n-1} * theta, beta_0) , if zeta * x_{n-1} <= epsilon_{n-1}
        ret.elasticBufferSize = std::max(ret.elasticBufferSize, config.beta0);

        // clip controlBlockSize to epsilonMax to avoid integer overflow for extreme sizes
        ret.controlBlockSize = std::min(ret.controlBlockSize, config.epsilonMax);
        // clip elasticBufferSize to betaMax to avoid integer overflow for extreme sizes
        ret.elasticBufferSize = std::min(ret.elasticBufferSize, config.betaMax);
    }
    return ret;
}

State State::lookaheadState(const Config &config, size_t count) {
    State lookaheadState = *this;
    for(size_t i = 0; i < count; i++) {
        uint64_t maxSize = lookaheadState.getBlockSizeLimit();
        lookaheadState = lookaheadState.nextABLAState(config, maxSize);
    }
    return lookaheadState;
}

bool State::IsValid(const Config &config, const char **err) const {
    if (controlBlockSize < config.epsilon0 || controlBlockSize > config.epsilonMax) {
        if (err) *err = "Error, invalid controlBlockSize state. Can't be below initialization value or above epsilonMax.";
        return false;
    }
    if (elasticBufferSize < config.beta0 || elasticBufferSize > config.betaMax) {
        if (err) *err = "Error, invalid elasticBufferSize state. Can't be below initialization value or above betaMax.";
        return false;
    }
    if (err) *err = "";
    return true;
}

} // namespace ABLA

static void printUsage(const char *progname)
{
    std::fprintf(stderr, "Usage: %s -excessiveblocksize 1000000"
                         " -ablaconfig beta0,n0,gammaReciprocal,zeta,thetaReciprocal,delta"
                         " [-ablacontinue blockHeight_n,elasticBufferSize_{n-1},controlBlockSize_{n-1}]"
                         " [-ablalookahead lookaheadCount]"
                         " [-disable2GBLimit]\n",
                 progname);
}

int main(int argc, char *argv[])
{
    uint64_t configInitialBlockSizeLimit{};
    ABLA::Config config;
    ABLA::State state;
    using std::string_view_literals::operator""sv;

    // Parse arguments
    if (argc < 3) {
        printUsage(argv[0]);
        return EXIT_FAILURE;
    }
    if (argv[1] == "-excessiveblocksize"sv) {
        if (std::sscanf(argv[2], "%" PRIu64, &configInitialBlockSizeLimit) != 1) {
            std::fprintf(stderr, "Error, -excessiveblocksize argument missing.\n");
            return EXIT_FAILURE;
        }
    }
    else {
        std::fprintf(stderr, "Error, failed parsing -excessiveblocksize argument.\n");
        return EXIT_FAILURE;
    }
    if (argc > 4 && argv[3] == "-ablaconfig"sv) {
        if (std::sscanf(argv[4], "%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64,
                        &config.beta0, &config.n0, &config.gammaReciprocal, &config.zeta_xB7,
                        &config.thetaReciprocal, &config.delta) != 6) {
            std::fprintf(stderr, "Error, failed parsing -ablaconfig arguments.\n");
            return EXIT_FAILURE;
        }

        // Finalize config
        if (configInitialBlockSizeLimit > config.beta0) {
            config.epsilon0 = configInitialBlockSizeLimit - config.beta0;
            config.SetMax();
        }
        else {
            std::fprintf(stderr, "Error, initial block size limit relative to initial elastic buffer size sanity check failed.\n");
            return EXIT_FAILURE;
        }

        // Sanity check the config
        const char *err{};
        if ( ! config.IsValid(&err)) {
            std::fprintf(stderr, "ABLA Config sanity check failed: %s\n", err);
            return EXIT_FAILURE;
        }

        // If we want to pick up from some height N instead of starting from height 0
        // then we must set the height & algorithm's state here.
        if (argc > 6 && argv[5] == "-ablacontinue"sv) {
            if (std::sscanf(argv[6], "%" PRIu64 ",%" PRIu64 ",%" PRIu64, &state.blockHeight, &state.elasticBufferSize, &state.controlBlockSize) != 3) {
                std::fprintf(stderr, "Error, failed parsing -ablacontinue arguments.\n");
                return EXIT_FAILURE;
            }

            // Sanity check the continued state object w.r.t. config
            if ( ! state.IsValid(config, &err)) {
                std::fprintf(stderr, "ABLA state sanity check failed: %s\n", err);
                return EXIT_FAILURE;
            }
        }
        else {
            // Initialize state for height 0
            state.blockHeight = 0;
            state.controlBlockSize = config.epsilon0;
            state.elasticBufferSize = config.beta0;
        }

        // Toggle to disable the 2GB limit
        if ((argc > 9 && argv[9] == "-disable2GBLimit"sv) ||
            (argc > 7 && argv[7] == "-disable2GBLimit"sv) ||
            (argc > 5 && argv[5] == "-disable2GBLimit"sv)
        ) {
            ABLA::TEMP_32_BIT_MAX_SAFE_BLOCKSIZE_LIMIT = std::numeric_limits<uint64_t>::max();
        }

        // If we want to look ahead, specify this argument and program
        // will immediately calculate, print, and exit
        if (argc > 8 && argv[7] == "-ablalookahead"sv) {
            size_t lookaheadCount;
            if (std::sscanf(argv[8], "%zu", &lookaheadCount) != 1) {
                std::fprintf(stderr, "Error, failed parsing -ablalookahead argument.\n");
                return EXIT_FAILURE;
            }
            ABLA::State lookaheadState = state.lookaheadState(config, lookaheadCount);
            std::printf("%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 "\n",
                state.blockHeight, state.getBlockSizeLimit(), state.elasticBufferSize, state.controlBlockSize,
                lookaheadCount,
                lookaheadState.blockHeight, lookaheadState.getBlockSizeLimit(), lookaheadState.elasticBufferSize, lookaheadState.controlBlockSize);

            return EXIT_SUCCESS;
        }
    }
    else {
        std::fprintf(stderr, "Error, -ablaconfig argument missing.\n");
        return EXIT_FAILURE;
    }

    // Calculate and print
    uint64_t blockSize;
    while (std::scanf("%" PRIu64, &blockSize) == 1) {
        // blockSize can't be greater than the limit, but if we're
        // testing against some dataset then we must clip it here.
        blockSize = std::min(blockSize, state.getBlockSizeLimit());
        // calculate the next block's algorithm state
        state = state.nextABLAState(config, blockSize);
        const uint64_t blockSizeLimit = state.getBlockSizeLimit();
        std::printf("%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 "\n",
                    state.blockHeight - 1u, blockSize, blockSizeLimit, state.elasticBufferSize,
                    state.controlBlockSize);
    }

    return EXIT_SUCCESS;
}
