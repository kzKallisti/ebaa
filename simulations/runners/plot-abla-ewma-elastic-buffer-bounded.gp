#!/usr/bin/gnuplot

#set output "$2"
#set title "$3"

set lt 1 lc rgb 'red'
set lt 2 lc rgb 'dark-blue'
set lt 3 lc rgb 'green'
set lt 4 lc rgb 'orange'
set lt 5 lc rgb 'blue'
set lt 6 lc rgb 'orange-red'
set lt 7 lc rgb 'yellow'
set lt 8 lc rgb 'violet'
set datafile separator ','
set decimalsign locale
set format y "%'.0f"
#set xdata time
set xlabel "Blockchain day (1 day = 144 blocks)"
set ylabel "Block size"
#set timefmt "%Y-%m-%d"
#set format x "%Y-%m"
set format x "%'.0f"
set key autotitle columnhead noenhanced
set key outside above
#set key left top vertical Left
set xtics out rotate autofreq 182.5
#set xtics 2629746
set autoscale xfix
#set xrange ["2022-10-01":*]
set term png size 640, 480
# Volume
set key autotitle columnhead
set grid
plot infile using ($1/144):7 with lines, infile using ($1/144):12 with lines, infile using ($1/144):17 with lines, infile using ($1/144):19 with lines, infile using ($1/144):21 with lines, infile using ($1/144):23 with lines
