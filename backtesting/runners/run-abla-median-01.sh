#!/bin/bash
bname="abla-median-01-blocksizes-bch"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../datasets/blocksizes-bch.csv" 1000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:4000000]" plot-abla-median.gp
) &

bname="abla-median-01-blocksizes-btc"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../datasets/blocksizes-btc.csv" 1000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:4000000]" plot-abla-median.gp
) &

bname="abla-median-01-chunksizes-eth"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../datasets/chunksizes-eth.csv" 1000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-chunksizes-ltc"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../datasets/chunksizes-ltc.csv" 1000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-hybrid-btc+ltc+eth+bch"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../datasets/hybrid-btc+ltc+eth+bch.csv" 1000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-hypothetical-eth-x64"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../datasets/hypothetical-eth-x64.csv" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &

bname="abla-median-01-hypothetical-hybrid-x64"
title="$bname"
(./run-abla-median.sh "../results/$bname.csv" "../datasets/hypothetical-hybrid-x64.csv" 32000000 0 256 12960
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'" plot-abla-median.gp
) &
